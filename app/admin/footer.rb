module ActiveAdmin
  module Views
    class Footer < Component

      def build
        super :id => "footer"                                                    
        super :style => "text-align: right;"                                     

        div do                                                                   
          small "This product is copyright of AdaptivIT Engineering #{Date.today.year}"                                       
        end
      end

    end
  end
end
